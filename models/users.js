const mongoose = require("mongoose");
var Schema = mongoose.Schema;

const UserSchema = mongoose.Schema({
  applicationid: {
    type: Number,
    required: true,
  },
  firstname: {
    type: String,
    required: true,
  },
  lastname: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    unique: true,
    lowercase: true,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  mobileno: {
    type: Number,
  },
  role: {
    type: String,
    required: true,
    enum: [
      "candidate",
      "Student",
      "Teacher",
      "Principal",
      "Administrator",
      "Alumuni",
      "Mentor",
    ],
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
  privileges: {
    read: {
      type: Boolean,
      default: false,
    },
    create: {
      type: Boolean,
      default: false,
    },
    update: {
      type: Boolean,
      default: false,
    },
    delete: {
      type: Boolean,
      default: false,
    },
  },
  created: {
    type: String,
    required: true,
  },
  request: {
    type: Boolean,
    default: false,
  },
});

const User = (module.exports = mongoose.model("User", UserSchema));
